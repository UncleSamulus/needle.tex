function needle_matrix(seq1, seq2)
    local gap_penalty = 3
    local mismatch_penalty = 2
    local match_penalty = 0
    local n1 = string.len(seq1)
    local n2 = string.len(seq2)
    -- Create a n1 x n2 matrix
    local table = {}
    for i=0,n1 do
        table[i] = {}
        for j=0,n2 do
            table[i][j] = 0
        end
    end
    -- Fill first row and first column
    for i=1,n1-1 do
        table[i][0] = i * gap_penalty 
    end
    for i=1,n2-1 do
        table[0][i] = i * gap_penalty
    end
    -- Fill the rest of the table
    local match, delete, insert
    for i=2,n1-1 do
        for j=2,n2-1 do
            if string.sub(seq1, i, i) == string.sub(seq2, j, j) then
                match = table[i-1][j-1] + match_penalty
            else
                match = table[i-1][j-1] + mismatch_penalty
            end
            delete = table[i-1][j] + gap_penalty
            insert = table[i][j-1] + gap_penalty
            table[i][j] = math.min(match, delete, insert)
        end
    end
    return table
end

-- tex.print(string.format(" Path: %s -> %s", seq1, seq2))
table = needle_matrix(seq1, seq2)
n1 = string.len(seq1)
n2 = string.len(seq2)
-- Draw the matrix as tikz nodes
for i=0,n1-1 do
    for j=0,n2-1 do
        tex.print(string.format("\\node[draw, minimum width=1cm, minimum height=1cm] at (%d, -%d) {};", i, j, table[i][j]))
    end
end
-- Draw the sequence labels
for i=1,n1 do
    tex.print(string.format("\\node at (%d, -%d) {%s};", i-1, -1, string.sub(seq1, i, i)))
end
for i=1,n2 do
    tex.print(string.format("\\node at (%d, -%d) {%s};", -1, i-1, string.sub(seq2, i, i)))
end
-- Add a path from the bottom right corner to the top left corner, following the minimum of the three possible moves at each step
local i, j, value, previous_value
i = n1-1
j = n2-1
tex.print(string.format("\\draw[-,line width=2, gray] (%d, -%d) --", i, j))
while i > 0 and j > 0 do 
    value = math.min(table[i-1][j-1], table[i-1][j], table[i][j-1])
    if value == table[i-1][j-1] then
        i = i - 1
        j = j - 1
    elseif value == table[i-1][j] then
        i = i - 1
    else
        j = j - 1
    end
    tex.print(string.format(" (%d, -%d) -- ", i, j))
end
tex.print(string.format("(0, 0) -- (-1, 1);", i, j))